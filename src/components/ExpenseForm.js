import { useState } from "react";
import {
    TextField,
    Button,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
} from "@mui/material";
import axios from "axios";

const addExpense = async (obj) => {
    const response = await axios.post("http://localhost:3001/expenses", obj);
    return response.data;
};

const ExpenseForm = ({ budget, categories, setExpenses }) => {

    const [category, setCategory] = useState("");
    const [amount, setAmount] = useState("");
    const [date, setDate] = useState("");
    const [description, setDescription] = useState("");

    const getCategoryId = (name) => {
        const result = categories.find((categ) => categ.name === name);
        return result.id;
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const expenseObject = {
            id: null,
            categoryId: getCategoryId(category),
            budgetId: budget.id,
            date: date,
            description: description,
            amount: amount,
        };
        addExpense(expenseObject)
            .then((responseData) => {
                setExpenses((prev) => {
                    return [...prev, responseData];
                });
                setDate("");
                setCategory("");
                setDescription("");
                setAmount("");
            })
            .catch((e) => console.log(e));
    };
    //Tässä esim. FromControl ja sen sisällä olevat asiat voisi muodostaa oman komponentin
    return (
        <div>
            <h2>Add an expense</h2>
            <form onSubmit={handleSubmit}>
                <TextField
                    required
                    id="date"
                    label="Date"
                    type="date"
                    variant="standard"
                    InputLabelProps={{ shrink: true }}
                    value={date}
                    onChange={(e) => setDate(e.target.value)}/>
                <FormControl sx={{width: 220, m : 2}}>
                    <InputLabel id="expense-category-label">Select category</InputLabel>
                    <Select
                        required
                        labelId="expense-category-label"
                        id="expense-category"
                        value={category}
                        label="Select category"
                        onChange={(e) => setCategory(e.target.value)}
                    >
                        { categories.map(categ => 
                            <MenuItem key={categ.id} value={categ.name}>{categ.name}</MenuItem>)}
                    </Select>
                </FormControl>
                <TextField
                    label="Description"
                    name="description"
                    variant="standard"
                    sx={{width: 220, m : 2}}
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                />
                <TextField
                    required
                    type="number"
                    InputLabelProps={{ shrink: true }}
                    inputProps={{ step: "0.01" }}
                    sx={{width: 220, m : 2}}
                    name="amount"
                    label="Amount"
                    value={amount}
                    placeholder="€"
                    variant="standard"
                    onChange={(e) => setAmount(e.target.value)}/>
                <Button
                    sx={{ m : 2}}
                    type="submit"
                    variant="contained"
                    color="primary"
                    size="large"
                >
                    Save
                </Button>
            </form>
        </div>
    );
};

export default ExpenseForm;
