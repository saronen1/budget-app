import {
    Collapse,
    IconButton,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
} from "@mui/material";
import {
    KeyboardArrowUp,
    KeyboardArrowDown,
    Delete,
} from "@mui/icons-material";
import { useState } from "react";

const Row = ({ data, remove, expenses, setExpenses }) => {
    const [open, setOpen] = useState(false);

    const deleteExpense = (id) => {
        remove("expenses", id)
            .then((response) => {
                if (response.status === 200) {
                    setExpenses(expenses.filter((exp) => exp.id !== id));
                }
            })
            .catch((e) => console.log(e));
    };

    //Mieti mitä eri rivit kuvaa ja lähde sen pohjalta miettimään, voisiko niistä tehdä omia komponentteja?
    return (
        <>
            <TableRow>
                <TableCell>
                    {data.expenses.length !== 0 && (
                        <IconButton
                            aria-label="expand row"
                            size="small"
                            onClick={() => setOpen(!open)}
                        >
                            {open ? <KeyboardArrowUp /> : <KeyboardArrowDown />}
                        </IconButton>
                    )}
                </TableCell>
                <TableCell>{data.categoryName}</TableCell>
                <TableCell>{data.limit ? data.limit : 0}</TableCell>
                <TableCell>{data.total}</TableCell>
                <TableCell>{Number(data.limit) - Number(data.total)}</TableCell>
            </TableRow>
            { data.expenses.length !== 0 &&
            <TableRow>
                <TableCell style={{ padding: 0 , backgroundColor: "#f5f5f5"}} colSpan={5}>
                    <Collapse in={open}>
                        <h3 style={{paddingLeft: "20px"}}>Expenses</h3>
                        <Table style={{ tableLayout: "fixed" }}>
                            <TableHead>
                                <TableRow>
                                    <TableCell />
                                    <TableCell>Date</TableCell>
                                    <TableCell>Description</TableCell>
                                    <TableCell>Amount</TableCell>
                                    <TableCell/>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                { data.expenses.map(expense => 
                                    <TableRow key={expense.id}>
                                        <TableCell/>
                                        <TableCell>{expense.date}</TableCell>
                                        <TableCell>{expense.description}</TableCell>
                                        <TableCell>{expense.amount} €</TableCell>
                                        <TableCell>
                                            <IconButton
                                                aria-label="delete-expense-button"
                                                size="small"
                                                onClick={() => deleteExpense(expense.id)}>
                                                <Delete/>
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>)}
                            </TableBody>
                        </Table>
                    </Collapse>
                </TableCell>
            </TableRow>
            }
        </>
    );
};

export default Row;
