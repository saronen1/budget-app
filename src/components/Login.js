import { Link as RouterLink } from "react-router-dom";

const Login = () => {
    return (
        <>
            <p>Login page</p>
            <RouterLink to="/register">Register</RouterLink>      
        </>
    );
};

export default Login;