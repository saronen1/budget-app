import { Link as RouterLink } from "react-router-dom";
import { AppBar, Box, Button, Container, Toolbar, Typography  } from "@mui/material";

const Navbar = () => {
    return (
        <Box>
            <AppBar position="static">
                <Toolbar>
                    <Container 
                        maxWidth="xl"
                        sx={{ display: "flex"}}>
                        <Typography
                            variant="h6"
                            sx={{ mr: 2 }}>
                            Budget App
                        </Typography>
                        <Button
                            disableElevation
                            variant="contained"
                            color="primary"
                            component={RouterLink}
                            to="/">
                                Home
                        </Button>
                        <Button
                            disableElevation
                            variant="contained"
                            color="primary"
                            component={RouterLink}
                            to="/login">
                                Login
                        </Button>
                    </Container>
                </Toolbar>
            </AppBar>
        </Box>
    );
};

export default Navbar;