import { useLocation, useNavigate } from "react-router-dom";
import axios from "axios";
import { useEffect, useState } from "react";
import {
    Box,
    Table,
    TableCell,
    TableRow,
    TableHead,
    TableBody,
    Typography,
    Button,
} from "@mui/material";
import { Delete } from "@mui/icons-material";
import ExpenseForm from "./ExpenseForm";
import Row from "./Row";

const getCategoryBudgets = async (budgetId) => {
    const response = await axios.get(
        `http://localhost:3001/category-budgets?budgetId=${budgetId}`
    );
    return response.data;
};

const getExpenses = async (budgetId) => {
    const response = await axios.get(
        `http://localhost:3001/expenses?budgetId=${budgetId}`
    );
    return response.data;
};

const remove = async (path, id) => {
    return await axios.delete(`http://localhost:3001/${path}/${id}`);
};

const BudgetPage = () => {
    const location = useLocation();
    const navigate = useNavigate();
    const [budget] = useState(location.state.budget);
    const [categories] = useState(location.state.categories);
    const [expenses, setExpenses] = useState([]);
    const [categoryBudgets, setCategoryBudgets] = useState([]);
    const [showForm, setShowForm] = useState(false);

    useEffect(() => {
        getCategoryBudgets(budget.id)
            .then((responseData) => setCategoryBudgets(responseData))
            .catch((e) => console.log(e));

        //Should these be chained?
        getExpenses(budget.id)
            .then((responseData) => setExpenses(responseData))
            .catch((e) => console.log(e));
    }, []);

    const calculateTotal = (expenses) => {
        let sum = 0;
        expenses.forEach((expense) => {
            sum += Number(expense.amount);
        });
        return sum.toString();
    };

    const createData = (obj) => {
        const category = categories.find(
            (categ) => categ.id === obj.categoryId
        );
        const filtered = expenses.filter(
            (expense) => expense.categoryId === obj.categoryId
        );
        return {
            categoryName: category.name,
            limit: obj.limit,
            expenses: filtered,
            total: calculateTotal(filtered),
        };
    };

    const deleteBudget = () => {
        // todo: error handling
        expenses.forEach((expense) => {
            remove("expenses", expense.id).catch((e) => console.log(e));
        });
        categoryBudgets.forEach((cb) => {
            remove("category-budgets", cb.id).catch((e) => console.log(e));
        });
        remove("budgets", budget.id)
            .then((response) => {
                if (response.status === 200) {
                    navigate("/");
                }
            })
            .catch((e) => console.log(e));
    };

    //Tässä esim. TableHead ja sen sisällä olevat asiat voisi muodostaa yhden komponentin
    //TableBody ja sen sisältö toisen.
    return (
        <Box mt={6}>
            <Typography variant="h4" component="h1">{budget.title}</Typography>
            <Table style={{ tableLayout: "fixed" }}>
                <TableHead>
                    <TableRow>
                        <TableCell />
                        <TableCell>Category</TableCell>
                        <TableCell>Budget</TableCell>
                        <TableCell>Spent</TableCell>
                        <TableCell>Balance</TableCell>
                    </TableRow>
                </TableHead>
                {categoryBudgets && (
                    <TableBody>
                        {categoryBudgets.map((obj) => (
                            <Row
                                key={obj.id}
                                data={createData(obj)}
                                remove={remove}
                                expenses={expenses}
                                setExpenses={setExpenses}
                            />
                        ))}
                        <TableRow>
                            <TableCell />
                            <TableCell>Total</TableCell>
                            <TableCell>{budget.amount}</TableCell>
                            <TableCell>{calculateTotal(expenses)}</TableCell>
                            <TableCell>
                                {budget.amount - calculateTotal(expenses)}
                            </TableCell>
                        </TableRow>
                    </TableBody>
                )}
            </Table>
            {showForm && (
                <ExpenseForm
                    budget={budget}
                    categories={categories}
                    expenses={expenses}
                    setExpenses={setExpenses}
                />
            )}
            <Box my={2}>
                <Button
                    sx={{mr: 1}}
                    variant="contained"
                    color="primary"
                    size="large"
                    onClick={() => setShowForm(!showForm)}
                >
                    {showForm ? "Close" : "New expense"}
                </Button>
                <Button
                    variant="contained"
                    startIcon={<Delete />}
                    color="secondary"
                    size="large"
                    onClick={() => deleteBudget(budget.id)}
                >
                Delete budget
                </Button>
            </Box>
        </Box>
    );
};

export default BudgetPage;
