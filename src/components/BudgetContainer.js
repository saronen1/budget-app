import axios from "axios";
import { Link as RouterLink } from "react-router-dom";
import { useEffect, useState } from "react";
import { Box, Button, List, ListItem, Link, Typography } from "@mui/material";
import NewBudgetForm from "./NewBudgetForm";

const getBudgets = async (id) => {
    const response = await axios.get(
        `http://localhost:3001/budgets?userId=${id}`
    );
    return response.data;
};

const getCategories = async () => {
    const response = await axios.get("http://localhost:3001/categories");
    return response.data;
};

const BudgetContainer = () => {
    const [budgets, setBudgets] = useState([]);
    const [categories, setCategories] = useState([]);
    const [showForm, setShowForm] = useState(false);

    useEffect(() => {
        // TODO: add user id, for now just fetching Test User's budgets
        getBudgets(1)
            .then((savedBudgets) => setBudgets(savedBudgets))
            .catch((e) => console.log(e));

        // should these be chained?
        getCategories()
            .then((categories) => setCategories(categories))
            .catch((e) => console.log(e));
    }, []);

    //Tässä listasta voisi tehdä oman komponenttinsa ja sen voi jopa jakaa kahteen:
    //Listan container ja listan item
    return (
        <Box>
            { showForm 
                ?
                <NewBudgetForm 
                    setBudgets={setBudgets}
                    categories={categories}
                    setShowForm={setShowForm}/>
                
                :<Button
                    sx={{mt : 2, mb: 4}}
                    variant="contained"
                    color="primary"
                    size="large"
                    onClick={() => setShowForm(true)}
                > New budget
                </Button>
            }
            <Typography gutterBottom variant="h5" component="h2">Your budgets</Typography>
            { budgets.length === 0 
                ? <Typography>You don&apos;t have any budgets yet.</Typography>
                :<List disablePadding={true}>
                    {budgets.map(budget =>
                        <ListItem key={budget.id}>
                            <Link 
                                variant="h6"
                                component={RouterLink}
                                to={`/budget/${budget.id}`}
                                state={{ budget, categories }}>
                                {budget.title}
                            </Link>
                        </ListItem>    
                    )}
                </List >
            }
        </Box>
    );
};

export default BudgetContainer;
