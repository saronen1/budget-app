import BudgetContainer from "./BudgetContainer";
import { Box, Typography } from "@mui/material";

const Home = () => {
    return (
        <Box mt={6}>
            <Typography gutterBottom variant="h4" component="h1">
                Welcome, user
            </Typography>
            <BudgetContainer/>
        </Box>
    );
};

export default Home;