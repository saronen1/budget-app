import { useEffect, useState } from "react";
import axios from "axios";
import { TextField, Button, Grid, Typography, Box } from "@mui/material";

const addBudget = async (obj) => {
    const response = await axios.post("http://localhost:3001/budgets", obj);
    return response.data;
};

const addCategoryBudget = async (obj) => {
    const response = await axios.post(
        "http://localhost:3001/category-budgets",
        obj
    );
    return response.data;
};

const calculateTotal = (obj) => {
    let sum = 0;
    Object.values(obj).forEach((value) => {
        sum += Number(value);
    });
    return sum;
};

const NewBudgetForm = ({ setBudgets, categories, setShowForm }) => {
    const initialValues = {
        housing: "",
        utilities: "",
        transport: "",
        groceries: "",
        pets: "",
        personal: "",
        entertainment: "",
        savings: "",
        miscellaneous: "",
    };

    const [title, setTitle] = useState("");
    const [total, setTotal] = useState(0);
    const [values, setValues] = useState(initialValues);

    useEffect(() => {
        setTotal(calculateTotal(values));
    }, [values]);

    const handleNumericalInputChange = (e) => {
        const { name, value } = e.target;
        const key = name.toLowerCase();
        setValues({
            ...values,
            [key]: value,
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        // todo: userId from user context
        // todo: what if title already exists?

        const budgetObj = {
            id: null,
            userId: 1,
            title: title,
            amount: total.toString(),
        };
        addBudget(budgetObj)
            .then((budgetData) => {
                setBudgets((prev) => {
                    return [...prev, budgetData];
                });
                categories.map(categ => {
                    const categoryBudgetObj = {
                        id: null,
                        budgetId: budgetData.id,
                        categoryId: categ.id,
                        limit: values[categ.name.toLowerCase()],
                    };
                    // todo: promise-chain these requests
                    addCategoryBudget(categoryBudgetObj)
                        .then((responseData) => console.log(responseData))
                        .catch((e) => console.log(e));
                });
            })
            .then(() => {
                setValues(initialValues);
                setTitle("");
                setShowForm(false);
            })
            .catch((e) => console.log(e));
    };

    //tätä voisi myös jakaa komponentteihin
    return (
        <form onSubmit={handleSubmit}>
            <Typography gutterBottom my={2} variant="h5" component="h2">New budget</Typography>
            <Grid container 
                sx={{
                    backgroundColor: "#e3f2fd"
                }}>
                <Grid container
                    p={2}
                    alignItems="flex-end"
                
                >
                    <Grid item xs={12} sm={8} lg={6} mb={1}>
                        <Typography variant="h6">
                        Budget Title
                        </Typography>
                    </Grid>
                    <Grid item xs={12} sm={4} lg={6}>
                        <TextField 
                            required
                            name="title"
                            variant="standard"
                            placeholder="E.g. January 2022"
                            value={title}
                            inputProps={{ sx : {p : 1}}}
                            sx={{ backgroundColor : "#ffffff"}}
                            onChange={(e) => setTitle(e.target.value)}/>
                    </Grid>
                </Grid> 
                {categories.map(categ => {
                    const categName = categ.name.toLowerCase();
                    return (
                        <Grid container 
                            key={categ.id}
                            alignItems="flex-end" 
                            sx={{ 
                                p : 2,
                                backgroundColor: (categ.id % 2 === 0) ? null : "#bbdefb"
                            }}>
                            <Grid item xs={12} sm={8} lg={6} mb={1}> 
                                <Typography variant="h6">
                                    {categ.name}
                                </Typography>
                                {categ.description}
                            </Grid>
                            <Grid item xs={12} sm={4} lg={6}>
                                <TextField
                                    type="number"
                                    InputLabelProps={{ shrink: true }}
                                    inputProps={{ 
                                        step: "0.01",
                                        sx: {p : 1}
                                    }}
                                    name={categ.name}
                                    value={values[categName]}
                                    placeholder="0"
                                    variant="standard"
                                    sx={{ backgroundColor : "#ffffff" }}
                                    onChange={handleNumericalInputChange}/>
                            </Grid>
                        </Grid>
                    );
                })}
                <Grid item xs={12} sm={8} lg={6} p={2} mb={1}>
                    <Typography variant="h6">
                    Total
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={4} lg={6} p={2}>
                    <Typography variant="h6">
                        {total} €
                    </Typography>
                </Grid>
            </Grid>
            <Box my={2}> 
                <Button
                    sx={{mr: 1} }
                    type="submit"
                    variant="contained"
                    color="primary"
                    size="large">
                    Save
                </Button>
                <Button
                    variant="contained"
                    color="secondary"
                    size="large"
                    onClick={() => setShowForm(false)}> 
                    Close
                </Button>
            </Box>
        </form>
    );
};

export default NewBudgetForm;
