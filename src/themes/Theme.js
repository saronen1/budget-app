import { createTheme } from "@mui/material/styles";

const theme = createTheme({
    palette: {
        secondary: {
            main: "#ef5350"
        }
    }
});

export default theme;

