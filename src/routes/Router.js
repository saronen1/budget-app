import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Container } from "@mui/material";
import Navbar from "../components/Navbar";
import Login from "../components/Login";
import Register from "../components/Register";
import Home from "../components/Home";
import BudgetPage from "../components/BudgetPage";

const Router = () => {
    return (
        <BrowserRouter>
            <Navbar />
            <Container maxWidth="xl">
                <Routes>
                    <Route path="/login" element={<Login />} />
                    <Route path="/register" element={<Register />} />
                    <Route path="/budget/:id" element={<BudgetPage />} />
                    <Route path="/" element={<Home />} />
                </Routes>
            </Container>
        </BrowserRouter>
    );
};

export default Router;
