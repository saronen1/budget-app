import Router from "./routes/Router";
import { ThemeProvider } from "@mui/material/styles";
import theme from "./themes/Theme";

const App = () => {
    return (
        <ThemeProvider theme={theme}>
            <Router />
        </ThemeProvider>
    );
};

export default App;
