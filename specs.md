# Specifications

## Description

The [app name here] app allows user to set a monthly budget using the envelope system. This means portioning the income to different categories such as housing, loans, groceries, phone bill, subcriptions, hobbies, shopping, etc. and tracking the money spent on each category.

*To be updated...*


## Functionality
### Minimum
- multiple views
    - login / register
    - front page / current month
        - monthly budget
            - money to spend (can be changed, default from personal settings?) on each category
            - expenses
            - balance (how much remaining / over the budget for each category)
                - visual?
- mock-database: db.json
    - users
    - budgets (or months)
        - related to user: usedId
    - category-budgets
        - includes the amounts assigned for each category in the monthly budget
        - connects categories with budgets (junction table): categoryId and budgetId
    - categories
    - expenses
        - related to categories: categoryId
        - related to budgets: budgetId

### Extra
- browse past months
- profile page / personal settings
    - set e.g. default monthly budget, currency (€ / $ / ...), theme...
- charts (separate view?)

## Styles
- material UI
- minimalistic