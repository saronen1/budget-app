*To be updated...*

# Budget App

## Description

This app is a React and Material UI rehearsal project, which was done as a part of Buutti Trainee Academy, a 9-month programming training. The purpose was to design and implement an app with a free topic in two weeks to practice e.g. application design, git workflow, React and use of a component library.

*The main idea: an app to plan and follow personal finances...*

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

![image.png](./image.png)
![image-1.png](./image-1.png)

## Functionality

###  Current features

The user can
- create budgets (e.g. for each month) by defining an amount of money they allocate for nine categories: 1. Housing, 2. Utilities, 3. Transport, 4. Groceries, 5. Pets, 6. Personal, 7. Entertainment, 8. Savings and 9. Miscellaneous
- see their budgets listed as links on the homepage
- view a budget and add expenses to each category
- see their total spending and balance for each category, and altogether
- delete expenses and budgets

The data is saved to a mock-database `db.json` using [JSON-server](https://github.com/typicode/json-server).

### Planned features
- visualization of the balances (positive => green, negative => red) and the percentage of budget that is left
- backend with node.js, express and postgreSQL
- authentication

## Installation

You need to have `node` and `npm` installed globally.\
Start by cloning the repository. In the project direcotry, run

`npm install`

to install dependencies,

`npm start`

to start the program and

`npm run server`

to start JSON-server. By default, the app will run in port 3000 and JSON-server in port 3001. If you need to change the port for JSON-server, find the following line in `package.json` and change 3001 to the port number of your choice, but in this case you will also need to change the address to any server requests in the code.

`"server": "json-server -p3001 db.json `

Open [http://localhost:3000](http://localhost:3000) to view app in the browser.
